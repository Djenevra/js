Task # 1:

Rewrite the "switch" into an "if"

Write the code using if..else which would correspond to the following switch:

switch (browser) {

  case 'Edge':

    alert( "You've got the Edge!" );

    break;

  case 'Chrome':

  case 'Firefox':

  case 'Safari':

  case 'Opera':

    alert( 'Okay we support these browsers too' );

    break;



  default:

    alert( 'We hope that this page looks ok!' );

}


Task 2:

Rewrite "if" into "switch"

Rewrite the code below using a single switch statement:

 let a = +prompt('a?', '');

if (a == 0) {

  alert( 0 );

}

if (a == 1) {

  alert( 1 );

}


if (a == 2 || a == 3) {

  alert( '2,3' );

} 





Link:

13.Switch https://javascript.info/switch