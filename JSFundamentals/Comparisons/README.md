What will be the result for these expressions?

5 > 4 // true

"apple" > "pineapple" // true

"2" > "12" // false

undefined == null // true

undefined === null // false

null == "\n0\n" // NaN

null === +"\n0\n" // false

Link:
8.Comparisons https://javascript.info/comparison