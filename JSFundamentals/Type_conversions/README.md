What are results of these expressions?

"" + 1 + 0 // "10"

"" - 1 + 0 // -1 

true + false // 1 

6 / "3" // 2 

"2" * "3" // 6

4 + 5 + "px" // "9px"

"$" + 4 + 5 // "$45"

"4" - 2 // 2

"4px" - 2 // NaN

7 / 0 // Infinity 

"  -9\n" + 5 // "-9/n"

"  -9\n" - 5 // NaN

null + 1 // 1

undefined + 1 // NaN

Link:
6.Type Conversions https://javascript.info/type-conversions