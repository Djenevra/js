Task # 1:

What are the final values of all variables a, b, c and d after the code below?

let a = 1, b = 1;

let c = ++a; // 2

let d = b++; // 1

Task # 2:

What are the values of a and x after the code below?

let a = 2;

let x = 1 + (a *= 2); // a = 4, x = 5

Link:

7.Operators https://javascript.info/operators