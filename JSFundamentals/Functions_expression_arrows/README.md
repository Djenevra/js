Rewrite with arrow functions

Replace Function Expressions with arrow functions in the code:

 function ask(question, yes, no) {

  if (confirm(question)) yes()

  else no();
}

ask(

  "Do you agree?",

  function() { alert("You agreed."); },

  function() { alert("You canceled the execution."); }

);

// ask(
  
  "Do you agree?",
  
 let yes = () => alert("You agreed."),
  
  let no = () => alert("You canceled the execution.")

);
ask()

Link:
15.Functions, Expressions, Arrows https://javascript.info/function-expressions-arrows