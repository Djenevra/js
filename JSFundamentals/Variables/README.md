Task # 1:

Working with variables

1.Declare two variables: admin and name.

2.Assign the value "John" to name.

3.Copy the value from name to admin.

4.Show the value of admin using alert (must output “John”).


Task # 2:

1.Create the variable with the name of our planet. How would you name such a variable? (earth)

2.Create the variable to store the name of the current visitor. How would you name that variable? (currentuser)


Link:

4.Variables https://javascript.info/variables