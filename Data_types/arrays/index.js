

//Task 1
extractCurrencyValue('$120');

function extractCurrencyValue(str) {
  return +str.slice(1);
}

// Task 2

let styles = ['Jazz', 'Blues'];
console.log(styles);
styles.push('Rock-n-Roll');
console.log(styles);

 
let middle = styles.length / 2;
console.log(' index Before trunc', middle);
middle = Math.trunc(middle);
console.log('Index after trunc', middle);
styles[middle] = 'Classics';
console.log( 'Check it', styles);

alert( styles.shift() );
console.log(styles);
styles.unshift('Rap', 'Reggae');
console.log(styles);

//Task 3
function sumInput() {

  let numbers = [];

  while (true) {

    let value = prompt("A number please?", 0);

    // should we cancel?
    if (value === "" || value === null || !isFinite(value)) break;

    numbers.push(+value);
  }

  let sum = 0;
  for (let number of numbers) {
    sum += number;
  }
  return sum;
}

alert( sumInput() );

