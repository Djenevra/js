Task 1:

Syntax check

What is the result of this code?

let user = {

  name: "John",

  go: function() { alert(this.name) }

}

(user.go)()

P.S. There’s a pitfall :)



Task 2:

Explain the value of "this"

In the code below we intend to call user.go() method 4 times in a row.

But calls (1) and (2) works differently from (3) and (4). Why?

 let obj, method;

obj = {

  go: function() { alert(this); }

};

obj.go();               // (1) [object Object]

(obj.go)();             // (2) [object Object]

(method = obj.go)();    // (3) undefined

(obj.go || obj.stop)(); // (4) undefined


Task 3:

Using "this" in object literal

Here the function makeUser returns an object.

What is the result of accessing its ref? Why?

function makeUser() {

  return {

    name: "John",

    ref: this

  };

};

let user = makeUser();

alert( user.ref.name ); // What's the result?


Task 4:

Create a calculator

Create an object calculator with three methods:

read() prompts for two values and saves them as object properties.

sum() returns the sum of saved values.

mul() multiplies saved values and returns the result.

let calculator = {

  // ... your code ...

};

calculator.read();

alert( calculator.sum() );

alert( calculator.mul() );



Task 5:

Chaining

There’s a ladder object that allows to go up and down:

let ladder = {

  step: 0,

  up() {

    this.step++;

  },

  down() {

    this.step--;

  },

  showStep: function() { // shows the current step

    alert( this.step );

  }

};

Now, if we need to make several calls in sequence, can do it like this:

ladder.up();

ladder.up();

ladder.down();

ladder.showStep(); // 1

Modify the code of up and down to make the calls chainable, like this:

ladder.up().up().down().showStep(); // 1

Such approach is widely used across JavaScript libraries.







Link:

1.Object method "this" https://javascript.info/object-methods